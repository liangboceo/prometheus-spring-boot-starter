package top.codef.config.servicemonitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import top.codef.config.annos.ConditionalOnServiceMonitor;
import top.codef.microservice.control.ServiceCheckControl;
import top.codef.microservice.events.ServiceDiscoveredListener;
import top.codef.microservice.events.ServiceInstanceLackEventListener;
import top.codef.microservice.events.ServiceInstanceUnhealthyEventListener;
import top.codef.microservice.events.ServiceLostEventListener;
import top.codef.microservice.interfaces.ServiceNoticeRepository;
import top.codef.properties.servicemonitor.ServiceMonitorProperties;

@Configuration
@ConditionalOnServiceMonitor
public class ServiceMonitorListenerConfig {

	@Autowired
	private ServiceMonitorProperties serviceMonitorProperties;

	@Bean
	public ServiceDiscoveredListener serviceExistedListener(ServiceCheckControl serviceCheckControl,
			DiscoveryClient discoveryClient, ApplicationEventPublisher publisher) {
		ServiceDiscoveredListener existedListener = new ServiceDiscoveredListener(serviceCheckControl,
				serviceMonitorProperties.getMonitorServices(), discoveryClient, publisher);
		return existedListener;
	}

	@Bean
	public ServiceInstanceLackEventListener serviceInstanceLackEventListener(
			ServiceNoticeRepository serviceNoticeRepository) {
		return new ServiceInstanceLackEventListener(serviceNoticeRepository);
	}

	@Bean
	public ServiceInstanceUnhealthyEventListener serviceInstanceUnhealthyEventListener(
			ServiceNoticeRepository serviceNoticeRepository) {
		return new ServiceInstanceUnhealthyEventListener(serviceNoticeRepository);
	}

	@Bean
	public ServiceLostEventListener serviceLostEventListener(ServiceNoticeRepository serviceNoticeRepository) {
		ServiceLostEventListener serviceLostEventListener = new ServiceLostEventListener(serviceNoticeRepository);
		return serviceLostEventListener;
	}
}
