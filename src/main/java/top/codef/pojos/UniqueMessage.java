package top.codef.pojos;

public interface UniqueMessage {

	/**
	 * 表示该条数据的唯一id
	 * 
	 * @return
	 */
	String getUid();

}
