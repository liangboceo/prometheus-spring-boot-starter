package top.codef.microservice.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import top.codef.microservice.interfaces.ServiceNoticeRepository;
import top.codef.notice.NoticeComponentFactory;
import top.codef.pojos.servicemonitor.MicroServiceReport;
import top.codef.pojos.servicemonitor.ServiceCheckNotice;
import top.codef.properties.PrometheusProperties;
import top.codef.text.NoticeTextResolverProvider;

public class ServiceNoticeTask implements Runnable {

	private final NoticeComponentFactory noticeComponentFactory;

	private final NoticeTextResolverProvider noticeTextResolverProvider;

	private final PrometheusProperties promethreusNoticeProperties;

	private final Log logger = LogFactory.getLog(ServiceNoticeTask.class);

	private final ServiceNoticeRepository serviceNoticeRepository;

	public ServiceNoticeTask(NoticeComponentFactory noticeComponentFactory,
			PrometheusProperties promethreusNoticeProperties, ServiceNoticeRepository serviceNoticeRepository,
			NoticeTextResolverProvider noticeTextResolverProvider) {
		this.noticeComponentFactory = noticeComponentFactory;
		this.noticeTextResolverProvider = noticeTextResolverProvider;
		this.promethreusNoticeProperties = promethreusNoticeProperties;
		this.serviceNoticeRepository = serviceNoticeRepository;
	}

	@Override
	public void run() {
		MicroServiceReport microServiceNotice = serviceNoticeRepository.report();
		if (microServiceNotice.isNeedReport()) {
			int problemCount = microServiceNotice.totalProblemCount();
			logger.debug("prepare for notice: \n " + microServiceNotice);
			ServiceCheckNotice serviceCheckNotice = new ServiceCheckNotice(microServiceNotice,
					promethreusNoticeProperties.getProjectEnviroment(), "服务监控通知");
			serviceCheckNotice.setProblemServiceCount(problemCount);
			noticeComponentFactory.get(promethreusNoticeProperties.getDefaultName()).forEach(
					x -> x.send(serviceCheckNotice, noticeTextResolverProvider.get(ServiceCheckNotice.class, x)));
		}
	}
}
